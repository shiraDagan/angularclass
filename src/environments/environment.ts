// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyCFyLopyyNgiLGbfJaEzt6tQGNCtqxkYbM",
    authDomain: "angular-class-34e70.firebaseapp.com",
    databaseURL: "https://angular-class-34e70.firebaseio.com",
    projectId: "angular-class-34e70",
    storageBucket: "angular-class-34e70.appspot.com",
    messagingSenderId: "146053975201",
    appId: "1:146053975201:web:d3df0c6f31309e295bf4e0",
    measurementId: "G-8ZVRMF5ZHE"
  }  
};


/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
