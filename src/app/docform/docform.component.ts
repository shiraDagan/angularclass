import { ClassifyService } from './../classify.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-docform',
  templateUrl: './docform.component.html',
  styleUrls: ['./docform.component.css']
})
export class DocformComponent implements OnInit {

  constructor(private classifyService:ClassifyService , private router:Router) { }

  url:string;
  text:string;
 
  onSubmit(){
    this.classifyService.doc = this.text;
    this.router.navigate(['/classified']);
  }

  ngOnInit() {
  }

}
