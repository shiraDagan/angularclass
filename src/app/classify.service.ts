import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ClassifyService {

  private url = "https://rniqbt3fbh.execute-api.us-east-1.amazonaws.com/beta";

  public catefories:object = {0: 'business', 1: 'entertainment', 2: 'politics', 3: 'sport', 4: 'tech'}//נקשר בין המספרים לקטגוריות
  public doc:string;

  classify():Observable<any>{
    let json = {
      "articles":[
        {"text":this.doc}//מקבל חלק ממאמר לכן doc

      ]
    }
let body = JSON.stringify(json);//לקיחת האוביקט והפיכתו לסטרינג. מה שיעבור תמיד זה יהיה ג'סון ולכן תמיד נצטרך לבצע stringify
return this.http.post<any>(this.url,body).pipe(
  map(res => {
    let final = res.body.replace('[','')//החלפת הסוגרים בכלום
    final = final.replace(']','')
    //נרצה לקבל מספר בלבד- רק ערך בלי הסוגריים
    return final;
  })
)
//הפוסט מחזיר any
  }

  constructor(private http:HttpClient) { }
}
