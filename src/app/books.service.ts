import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AngularFirestore } from '@angular/fire/firestore';

 

@Injectable({
  providedIn: 'root'
})
export class BooksService {
 
 // books: any =  [{title:'Alice in Wonderland', author:'Lewis Carrol'},{title:'War and Peace', author:'Leo Tolstoy'}, {title:'The Magic Mountain', author:'Thomas Mann'}]

/*  addBooks(){
    setInterval(() => 
      this.books.push({title:'A new one', author:'New author'})
  , 5000);    
  }
/*
  getBooks(): any {
    const booksObservable = new Observable(observer => {
           setInterval(() => 
               observer.next(this.books)
           , 5000);
    });

    return booksObservable;
}
*/
  
  getBooks():Observable<any[]>{
    return this.db.collection('books').valueChanges({idField:'id'});
  }
  
  addBook(title:string,author:string){
    const book = {title:title, author:author};
  this.db.collection('books').add(book)
  }
deletebooks(id:string){
  this.db.doc(`books/${id}`).delete();
}
updateBook(id:string,title:string,author:string){
this.db.doc(`books/${id}`).update({title:title,author:author});
}
getBook(id:string):Observable<any>{
  return this.db.doc(`books/${id}`).get();
}
  constructor(private db:AngularFirestore) { }
}