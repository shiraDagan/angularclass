import { ImageService } from './../image.service';
import { ClassifyService } from './../classify.service';
import { Component, OnInit } from '@angular/core';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'app-classified',
  templateUrl: './classified.component.html',
  styleUrls: ['./classified.component.css']
})
export class ClassifiedComponent implements OnInit {

  category:string = "Loading.."
  categoryImage:string;

  constructor(public classifyService:ClassifyService, public imageService:ImageService) { }

  ngOnInit() {
    this.classifyService.classify().subscribe(
      res =>{//מספר מ 0 עד 4
        this.category = this.classifyService.catefories[res];
        this.categoryImage = this.imageService.images[res];
      }

    )
  }

}
