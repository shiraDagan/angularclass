import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { BooksService } from './../books.service';
@Component({
  selector: 'app-addbooks',
  templateUrl: './addbooks.component.html',
  styleUrls: ['./addbooks.component.css']
})
export class AddbooksComponent implements OnInit {
title:string;
author:string;
id:string;
isEdit:boolean=false;
buttonText:string = "Add book";
  constructor(private router: Router,private route:ActivatedRoute, private bookservice:BooksService) { }

  onSubmit() {
    if (this.isEdit){
      this.bookservice.updateBook(this.id,this.author,this.title);
    }
    else
    {
  this.bookservice.addBook(this.title,this.author);
  console.log(this.title);
}
 this.router.navigate(['/books']);
  }
  ngOnInit() {
    this.id=this.route.snapshot.params.id;
  console.log(this.id);
  if(this.id){
this.isEdit = true;
this.buttonText = "update book";
this.bookservice.getBook(this.id).subscribe(
  book => {
    console.log(book.author);
    console.log(book.title);
    this.author = book.data().author;//בוק נקודה אוטר מהדטה בייס
    this.title = book.data().title;
  }
)
  }
  }
}
