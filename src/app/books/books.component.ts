import { Observable } from 'rxjs';
import { BooksService } from './../books.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {

  panelOpenState = false;
  //books: object[] =  [{title:'Alice in Wonderland', author:'Lewis Carrol'},{title:'War and Peace', author:'Leo Tolstoy'}, {title:'The Magic Mountain', author:'Thomas Mann'}]
  //books: object[];
  books$:Observable<any>;
  deleteBook(id:string){
    this.bookservice.deletebooks(id);
  }
  /*updateBook(title:string,author:string){
    this.bookservice.updateBook({title,author});
  }*/
  constructor(private bookservice:BooksService) { }

  ngOnInit() {
    //this.books = this.bookservice.getBooks();
    /*
    this.bookservice.getBooks().subscribe(
      (books) => {this.books = books}
    )
    */
   // this.bookservice.addBooks();
    this.books$ = this.bookservice.getBooks();
  }

}
